﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObject : MonoBehaviour
{
	public MeshRenderer[] meshRend;
	public Rigidbody rigid;
	
	
	public static PickupObject CurrentRight, CurrentLeft;
	public enum WHICH_HAND { None, Left, Right };
	protected WHICH_HAND currentHandHovering;
	
	protected Transform originalParent;
	
	public virtual void OnTrigger(bool isRight)
	{
	}
	
	public virtual void OnTriggerUpdate(bool isRight)
	{
	}
	
	
    // Start is called before the first frame update
    void Start()
    {
        originalParent = transform.parent;
		OnStart();
    }
	
	protected virtual void OnStart()
	{
	}

	public void OnHover(bool rightHand)
	{
		if(currentHandHovering != WHICH_HAND.None)  //Already hovering
			return;
		if(currentHandHovering == WHICH_HAND.Right && CurrentRight != null)
			return;
		if(currentHandHovering == WHICH_HAND.Left && CurrentLeft != null)
			return;
		for(int i = 0; i < meshRend.Length; i++)
		{
			meshRend[i].material.SetColor("_EmissionColor", new Color(0.2f, 0.2f, 0f, 1f));
		}
		if(rightHand)
			currentHandHovering = WHICH_HAND.Right;
		else
			currentHandHovering = WHICH_HAND.Left;
	}
	
	public void OnEndHover(bool rightHand)
	{
		
		for(int i = 0; i < meshRend.Length; i++)
		{
			meshRend[i].material.SetColor("_EmissionColor", new Color(0f, 0f, 0f, 1f));
		}
		
		currentHandHovering = WHICH_HAND.None;
		
		if(CurrentLeft == this)
		{
			transform.parent = originalParent;
			CurrentLeft = null;
		}
		if(CurrentRight == this)
		{
			transform.parent = originalParent;
			CurrentRight = null;
		}
	}

	protected virtual void OnUpdate()
	{
	}
    // Update is called once per frame
    void FixedUpdate()
    {
		
        if(currentHandHovering == WHICH_HAND.Right)
		{
			if(CrossVR.hands[CrossVR.Right].gripPressed)
			{
				CurrentRight = this;
				transform.parent = CrossVR.RightHandTransform;
			}
			else
			{
				CurrentRight = null;
				transform.parent = originalParent;
			}
		}
		if(currentHandHovering == WHICH_HAND.Left)
		{
			if(CrossVR.hands[CrossVR.Left].gripPressed)
			{
				CurrentLeft = this;
				transform.parent = CrossVR.LeftHandTransform;
			}
			else
			{
				CurrentLeft = null;
				transform.parent = originalParent;
			}
		}
		
		if(transform.parent != originalParent)
		{
			rigid.constraints = RigidbodyConstraints.FreezeAll;
		}
		else
		{
			rigid.constraints = RigidbodyConstraints.None;
		}
		
		if(CurrentLeft == this)
		{
			if(CrossVR.hands[CrossVR.Left].triggerPressed)
				OnTriggerUpdate(false);
			if(CrossVR.hands[CrossVR.Left].triggerDown)
				OnTrigger(false);
		}
		
		if(CurrentRight == this)
		{
			if(CrossVR.hands[CrossVR.Right].triggerPressed)
				OnTriggerUpdate(true);
			if(CrossVR.hands[CrossVR.Right].triggerDown)
				OnTrigger(true);
		}
		
		OnUpdate();
    }
}

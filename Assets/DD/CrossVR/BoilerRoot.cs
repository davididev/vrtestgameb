﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoilerRoot : MonoBehaviour
{
	public static int Count = 0;
    // Start is called before the first frame update
    void Awake()
    {
		Count++;
		if(Count > 1)
		{
			Destroy(this.gameObject);
			return;
		}
        Object.DontDestroyOnLoad(transform);
        //Load, but only if in Editor
#if UNITY_EDITOR
        //GameDataHolder.LoadFile(GameDataHolder.fileID);  
#endif
    }

    public static void DestroyMe()
    {
		BoilerRoot br = FindObjectOfType<BoilerRoot>();
		if(br != null)
			Destroy(br.gameObject);
    }
	
	void OnDestroy()
	{
		Count--;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}

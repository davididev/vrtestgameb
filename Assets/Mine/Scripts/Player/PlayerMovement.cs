﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	private CharacterControllerMovement ccm;
	public Transform headRotator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(ccm == null)
			ccm = GetComponent<CharacterControllerMovement>();
		
		Vector2 joy = CrossVR.hands[CrossVR.Left].joystick;
		ccm.moveVec = joy;
		
		if(CrossVR.hands[CrossVR.Left].thumbDown)
		{
			ccm.AddForce(new Vector3(joy.x, 0f, joy.y) * 4f);
			ccm.Jump(10f);
		}
		
		Vector3 ang = transform.eulerAngles;
		ang.y = HeadRotator.RealEulerAngles.y;
		transform.eulerAngles = ang;
		headRotator.position = transform.position + Vector3.up;
    }
}

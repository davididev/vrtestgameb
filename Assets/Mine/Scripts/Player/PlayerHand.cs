﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHand : MonoBehaviour
{
	public bool isRightHand = false;
	private Animator anim;
	
	int handID = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(isRightHand)
			handID = CrossVR.Right;
		else
			handID = CrossVR.Left;
    }

    // Update is called once per frame
    void Update()
    {
		if(anim == null)
			anim = GetComponent<Animator>();
		
		/*
        CrossVR.HandInfo info;
		if(isRightHand)
			info = CrossVR.hands[CrossVR.Right];
		else
			info = CrossVR.hands[CrossVR.Left];
		*/
		
		//Debug.Log("Grip for " + handID + ": " + CrossVR.hands[handID].gripPressed);
		anim.SetBool("Grab", CrossVR.hands[handID].gripPressed);
    }
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Grab")
		{
			c.attachedRigidbody.GetComponent<PickupObject>().OnHover(isRightHand);
		}
	}
	
	void OnTriggerExit(Collider c)
	{
		if(c.tag == "Grab")
		{
			c.attachedRigidbody.GetComponent<PickupObject>().OnEndHover(isRightHand);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teapot : PickupObject
{
	public GameObject teaDrop;
	private float teaDropTimer = 0f;
	private const float TEA_DROP_TIME = 0.001f, DROP_SIZE = 0.0025f * 20f;
	
	public Transform dropPoint;
    // Start is called before the first frame update
    protected override void OnStart()
    {
        GameObjectPool.InitPoolItem("TeaDrop", teaDrop, 250);
    }

    // Update is called once per frame
    protected override void OnUpdate()
    {
        if(teaDropTimer > 0f)
			teaDropTimer -= Time.fixedDeltaTime;
    }
	
	public override void OnTriggerUpdate(bool isRight)
	{
		if(teaDropTimer <= 0f)
		{
			teaDropTimer = TEA_DROP_TIME;
			for(float i = 1f; i < 4f; i++)
			{
				Vector3 v = dropPoint.position;
				v = v + (dropPoint.forward * (DROP_SIZE * i));
				GameObject g = GameObjectPool.GetInstance("TeaDrop", v, Quaternion.identity);
				if(g != null)
				{
					g.GetComponent<Rigidbody>().AddForce(dropPoint.forward * 0.1f, ForceMode.VelocityChange);
				}
			}
				
		}
	}
}

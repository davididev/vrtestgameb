﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teadrop : MonoBehaviour
{
	public AudioClip gulpFX, splashFX;
    // Start is called before the first frame update
    void OnEnable()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if(transform.position.y < -10f)
			gameObject.SetActive(false);
    }
	
	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.layer == 0) //Default layer- deactivate
		{
			AudioSource.PlayClipAtPoint(splashFX, HeadRotator.RealPosition);
			gameObject.SetActive(false);
		}
		
		if(col.gameObject.tag == "Player")
		{
			AudioSource.PlayClipAtPoint(gulpFX, HeadRotator.RealPosition);
			gameObject.SetActive(false);
		}
	}
}
